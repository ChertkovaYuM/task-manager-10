package ru.tsc.chertkova.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProject();

    void createProject();

}
