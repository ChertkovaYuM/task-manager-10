package ru.tsc.chertkova.tm.api.repository;

import ru.tsc.chertkova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    void remove(Project project);

    void clear();

}
